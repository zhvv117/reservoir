import argparse
import json
import numpy as np
from sklearn.linear_model import Lasso

from common import read_tsv, build_index, NAME_KEY, DATE_KEY, X_KEY, Y_KEY, EPS


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", required=True, help="Well data in tsv format")
    parser.add_argument("-o", "--output", required=True, help="Output JSON")
    parser.add_argument("-c", "--coords", required=True, help="Well coodinates in tsv format")
    parser.add_argument("-l", "--coeff", type=float, default=0.1, required=False, help="Lasso coefficient")
    return parser.parse_args()


def main():
    args = parse_args()

    data_raw = read_tsv(args.input)

    coords = {}
    for record in read_tsv(args.coords):
        coords[record["№ скважины"]] = (float(record["Координата X"]), float(record["Координата Y"]))

    name2feature = {}
    feature2name = {}
    next_val = 0
    for record in data_raw:
        name = record[NAME_KEY]
        if name not in name2feature:
            name2feature[name] = next_val
            feature2name[next_val] = name
            next_val += 1

    data_raw_name_index = build_index(data_raw, NAME_KEY)
    data_raw_date_index = build_index(data_raw, DATE_KEY)

    influence = {}
    for name in data_raw_name_index:
        x_train = []
        y_train = []
        for record in data_raw_name_index[name]:
            y = float(record[Y_KEY])
            date = record[DATE_KEY]

            if y < EPS:
                continue

            features = np.zeros(len(name2feature))
            for record2 in data_raw_date_index[date]:
                x = float(record2[X_KEY])
                if x < EPS:
                    continue
                features[name2feature[record2[NAME_KEY]]] = x

            x_train.append(features)
            y_train.append(y)

        if len(y_train) < 30:
            continue

        clf = Lasso(alpha=args.coeff)
        clf.fit(x_train, y_train)
        influence[name] = []
        for index in clf.coef_.nonzero()[0]:
            influence[name].append(feature2name[index])

    with open(args.output, "w") as f:
        json.dump(influence, f)


if __name__ == "__main__":
    main()
