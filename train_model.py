import argparse
import json
import numpy as np
import pickle
from xgboost import XGBRegressor

from common import read_tsv, build_index, tfloat, calc_dist, NAME_KEY, DATE_KEY, X_KEY, Y_KEY


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", required=True, help="Well data in tsv format")
    parser.add_argument("-o", "--output", required=True, help="Output model")
    parser.add_argument("-c", "--coords", required=True, help="Well coodinates in tsv format")
    parser.add_argument("-f", "--influence", required=True, help="Influences in JSON format")
    parser.add_argument("--cv", default=False, action="store_true", required=False, help="Enable cross validation")
    return parser.parse_args()


def main():
    args = parse_args()

    data_raw = read_tsv(args.input)
    data_raw_date_index = build_index(data_raw, DATE_KEY)

    coords = {}
    for record in read_tsv(args.coords):
        coords[record["№ скважины"]] = (float(record["Координата X"]), float(record["Координата Y"]))

    with open(args.influence) as f:
        influence = json.load(f)

    x_train = []
    y_train = []
    x_test = []
    y_test = []

    for record in data_raw:
        y = float(record[Y_KEY])
        date = record[DATE_KEY]
        name = record[NAME_KEY]
        coord = coords.get(name)
        h_sp = tfloat(record["Глубина спуска"])

        if y < 1e-6 or coord is None:
            continue

        features = 33 * [0.0]

        for record2 in data_raw_date_index[date]:
            if name in influence and record2[NAME_KEY] not in influence[name]:
                continue

            x = float(record2[X_KEY])
            coord2 = coords.get(record2[NAME_KEY])
            if x < 1e-6 or coord2 is None:
                continue

            dist = calc_dist(coord, coord2) + 1
            h_sp2 = tfloat(record2["Н сп"])

            features[0] += x
            features[1] += x / dist
            features[2] += x / (dist ** 0.5)
            features[3] += x / (dist ** 2)
            features[4] += x / (dist ** 3)
            features[5] += x / (dist ** 4)

            if h_sp2 >= h_sp:
                features[6] += x / dist
                features[7] += x / (dist ** 0.5)
                features[8] += x / (dist ** 2)
                features[9] += x / (dist ** 3)
                features[10] += x / (dist ** 4)
                features[11] += x

            if dist < 2000:
                features[21] += x / dist
                features[22] += x / (dist ** 0.5)
                features[23] += x / (dist ** 2)
                features[24] += x / (dist ** 3)
                features[25] += x / (dist ** 4)
                features[26] += x

            if dist < 2000 and h_sp2 >= h_sp:
                features[27] += x / dist
                features[28] += x / (dist ** 0.5)
                features[29] += x / (dist ** 2)
                features[30] += x / (dist ** 3)
                features[31] += x / (dist ** 4)
                features[32] += x

        features[12] = tfloat(record["Пластовое давление"])
        features[13] = tfloat(record["Забойное давление"])
        features[14] = tfloat(record["Производительность ЭЦН"])
        features[15] = tfloat(record["Напор"])
        features[16] = tfloat(record["Частота"])
        features[17] = tfloat(record["Буферное давление"])
        features[18] = tfloat(record["Давление в линии"])
        features[19] = tfloat(record["Давление на приеме"])
        features[20] = tfloat(record["Радиус контура питания"])

        if args.cv and hash(record[NAME_KEY]) % 10 < 2:
            x_test.append(features)
            y_test.append(y)
        else:
            x_train.append(features)
            y_train.append(y)

    clf = XGBRegressor(n_estimators=100, max_depth=4)
    clf.fit(x_train, y_train)
    y_pred = clf.predict(x_train)
    print("MSE on train: {}".format(((y_pred - np.array(y_train)) ** 2).mean() ** 0.5))

    if args.cv:
        y_pred = clf.predict(x_test)
        print("MSE on test: {}".format(((y_pred - np.array(y_test)) ** 2).mean() ** 0.5))

    with open(args.output, "wb") as f:
        pickle.dump(clf, f)


if __name__ == "__main__":
    main()
