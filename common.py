from collections import defaultdict


INDEX_KEY = "Index"
NAME_KEY = "Скважина"
DATE_KEY = "Дата"
X_KEY = "Закачка, м3"
Y_KEY = "Нефть, т"
EPS = 1e-6


def read_file(filename):
    with open(filename) as f:
        for line in f:
            yield line.rstrip("\r\n")


def read_tsv(filename):
    train_f = read_file(filename)
    header = next(train_f).split("\t")
    train_raw = []
    for i, line in enumerate(train_f):
        sample = {
            INDEX_KEY: i
        }
        for k, v in zip(header, line.split("\t")):
            sample[k] = v
        train_raw.append(sample)
    return train_raw


def build_index(data, key):
    index = defaultdict(list)
    for record in data:
        index[record[key]].append(record)
    return index


def calc_dist(coord1, coord2):
    x1, y1 = coord1
    x2, y2 = coord2
    return ((x2 - x1) ** 2 + (y2 - y1) ** 2) ** 0.5


def tfloat(x):
    try:
        return float(x)
    except ValueError:
        return 0.0
