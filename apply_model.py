import argparse
import json
import pickle

from common import read_tsv, build_index, tfloat, calc_dist, NAME_KEY, DATE_KEY, X_KEY, Y_KEY


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", required=True, help="Well data in tsv format")
    parser.add_argument("-o", "--output", required=True, help="Output predictions")
    parser.add_argument("-c", "--coords", required=True, help="Well coodinates in tsv format")
    parser.add_argument("-m", "--model", required=True, help="XGBoost model")
    parser.add_argument("-f", "--influence", required=True, help="Influences in JSON format")
    parser.add_argument("-s", "--multiplier", type=float, default=1.0, required=False, help="Multiplier for water")
    return parser.parse_args()


def main():
    args = parse_args()

    data_raw = read_tsv(args.input)
    data_raw_date_index = build_index(data_raw, DATE_KEY)

    coords = {}
    for record in read_tsv(args.coords):
        coords[record["№ скважины"]] = (float(record["Координата X"]), float(record["Координата Y"]))

    with open(args.model, "rb") as f:
        clf = pickle.load(f)

    with open(args.influence) as f:
        influence = json.load(f)

    result_sum = 0.0
    for record in data_raw:
        record[Y_KEY] = 0.0
        date = record[DATE_KEY]
        name = record[NAME_KEY]
        coord = coords.get(name)
        h_sp = tfloat(record["Глубина спуска"])

        if coord is None:
            continue

        features = 33 * [0.0]

        for record2 in data_raw_date_index[date]:
            if name in influence and record2[NAME_KEY] not in influence[name]:
                continue

            x = args.multiplier * float(record2[X_KEY])
            coord2 = coords.get(record2[NAME_KEY])
            if x < 1e-6 or coord2 is None:
                continue

            dist = calc_dist(coord, coord2) + 1
            h_sp2 = tfloat(record2["Н сп"])

            features[0] += x
            features[1] += x / dist
            features[2] += x / (dist ** 0.5)
            features[3] += x / (dist ** 2)
            features[4] += x / (dist ** 3)
            features[5] += x / (dist ** 4)

            if h_sp2 >= h_sp:
                features[6] += x / dist
                features[7] += x / (dist ** 0.5)
                features[8] += x / (dist ** 2)
                features[9] += x / (dist ** 3)
                features[10] += x / (dist ** 4)
                features[11] += x

            if dist < 2000:
                features[21] += x / dist
                features[22] += x / (dist ** 0.5)
                features[23] += x / (dist ** 2)
                features[24] += x / (dist ** 3)
                features[25] += x / (dist ** 4)
                features[26] += x

            if dist < 2000 and h_sp2 >= h_sp:
                features[27] += x / dist
                features[28] += x / (dist ** 0.5)
                features[29] += x / (dist ** 2)
                features[30] += x / (dist ** 3)
                features[31] += x / (dist ** 4)
                features[32] += x

        features[12] = tfloat(record["Пластовое давление"])
        features[13] = tfloat(record["Забойное давление"])
        features[14] = tfloat(record["Производительность ЭЦН"])
        features[15] = tfloat(record["Напор"])
        features[16] = tfloat(record["Частота"])
        features[17] = tfloat(record["Буферное давление"])
        features[18] = tfloat(record["Давление в линии"])
        features[19] = tfloat(record["Давление на приеме"])
        features[20] = tfloat(record["Радиус контура питания"])

        record[Y_KEY] = clf.predict([features])[0]
        result_sum += record[Y_KEY]

    print("Result sum: {}".format(result_sum))

    with open(args.output, "w") as f:
        header = [DATE_KEY, NAME_KEY, Y_KEY]
        print("\t".join(header), file=f)
        for record in data_raw:
            print("\t".join([str(record[key]) for key in header]), file=f)


if __name__ == "__main__":
    main()
