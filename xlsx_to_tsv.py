import argparse
import pandas as pd


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", required=True)
    parser.add_argument("-o", "--output", required=True)
    parser.add_argument("-c", "--coords", required=True)
    return parser.parse_args()


def main():
    args = parse_args()

    data_xls = pd.read_excel(args.input, sheet_name=0)
    data_xls.to_csv(args.output, encoding="utf-8", index=False, sep="\t")

    data_xls = pd.read_excel(args.input, sheet_name=1)
    data_xls.to_csv(args.coords, encoding="utf-8", index=False, sep="\t")


if __name__ == "__main__":
    main()
