#!/bin/bash

mkdir -p ./tmp

python xlsx_to_tsv.py -i ./data/train.xlsx -o ./tmp/train.tsv -c ./tmp/train_coords.tsv
python xlsx_to_tsv.py -i ./data/test.xlsx -o ./tmp/test.tsv -c ./tmp/test_coords.tsv

python influence.py -i ./tmp/train.tsv -c ./tmp/train_coords.tsv -o ./tmp/influence.json -l 0.1

python train_model.py -i ./tmp/train.tsv -c ./tmp/train_coords.tsv -o ./tmp/model.pkl -f ./tmp/influence.json

python apply_model.py -i ./tmp/test.tsv -c ./tmp/test_coords.tsv -o ./tmp/test_predicted.tsv -m ./tmp/model.pkl -f ./tmp/influence.json
